from quspin.operators import hamiltonian # Hamiltonians and operators
from quspin.basis import spin_basis_1d # Hilbert space spin basis
import numpy as np # generic math functions
from time import time
from quspin.tools.measurements import ent_entropy, diag_ensemble
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
fig = plt.figure()
ax = fig.add_subplot()



# This code is just to check if the rate functions we obtain are intensive or not.




hz=0.01*0 # z external field
J = 1.0 # xy interaction
Jzz_0 = 1.0 # zz interaction
vs=np.logspace(-2.0, 0.0, num=20, base=10)


h0 = 1.1
hf = 0.9


s = 5
l = 2
global v

t_f = 0.1
v = (hf-h0)/t_f


RF =  [float(0.0) for y in range(s)]
TF =  [float(t_f) for y in range(s)]

def R_F(L):
    
    def ramp(t):
        return (h0 + v*t)
    ramp_args=[]

#ti = time() # get start time



# compute basis in the 0-total magnetisation sector (requires L even)
    #basis = spin_basis_1d(L,pauli=False,zblock = +1)
    basis = spin_basis_1d(L,pauli=False,zblock = +1,kblock = 0,pblock = 1)
# define operators with OBC using site-coupling lists
    h_field=[[-1,i] for i in range(L)]
    J_zz=[[-J,i,(i+1)%L] for i in range(L)] # PBC
    h_z=[[hz,i] for i in range(L)]
# static and dynamic lists
    static = [["z",h_z],["zz",J_zz]]
    dynamic =[["x",h_field,ramp,ramp_args]]
# compute the time-dependent Heisenberg Hamiltonian
    H_Ising = hamiltonian(static,dynamic,basis=basis,dtype=np.float64,check_symm = False)

    E0,psi_0=H_Ising.eigsh(time = 0.0,k=1,which = "SA",maxiter=1E6)
    

    E00,psi_0f=H_Ising.eigsh(time = t_f,k=1,which = "SA",maxiter=1E6)

#psi_0 = psi_0.reshape((-1,))
    norm = np.linalg.norm(psi_0)
    psi_0 = psi_0/norm

    psi = H_Ising.evolve(psi_0, 0.0 ,t_f)
    norm1 = np.linalg.norm(psi)
    psi = psi/norm1



    norm2 = np.linalg.norm(psi_0f)
    psi_0f = psi_0f/norm2
    return -np.log(np.absolute(np.sum(np.conjugate(psi) * psi_0f)))/L

for i in range(s):
    RF[i] = R_F(12+2*i)


Size = [12,14,16,18,20]


def funcE(z,a,b,c):
    return a*z+b+c/z

def funcI(z,d,e,f):
    return d + e/z + f/(z)**2



leg = ax.legend(prop={"size":30})
for label in (ax.get_xticklabels() + ax.get_yticklabels()):
	label.set_fontsize(16)


valuesE,errorsE = curve_fit(funcE,Size,RF)
a,b,c = valuesE
x = np.linspace(10,24,1000)

lE=a*x+b+c/x

print(a,b,c)
perrE = np.sqrt(np.diag(errorsE))

print(perrE)


valuesI,errorsI = curve_fit(funcI,Size,RF)
d,e,f = valuesI

lI = d + e/x + f/(x)**2

print(d,e,f)
perrI = np.sqrt(np.diag(errorsI))

print(perrI)


box_style=dict(boxstyle='round', facecolor='wheat', alpha=0.5)
plt.plot(x, lI,label = 'Intensive',color='g')
plt.plot(x, lE,label = 'Extensive',color='b')
#ax.axvline(x=0, color='k')
plt.plot(Size,RF,'--ro',label = 'Rate Function')#label='L = 08')
#plt.text(0.125,0.00175, "$y=ax + b,  a = %s, b=%d$"%(a,b),{'color':'red','weight':'heavy','size':20},bbox=box_style)

box_style=dict(boxstyle='round', facecolor='wheat', alpha=0.5)
#plt.plot(x, l)
#ax.axvline(x=0, color='k')
plt.plot(Size,RF,'--ro')#label='L = 08')
#plt.text(0.125,0.00175, "$y=ax + b,  a = %s, b=%d$"%(a,b),{'color':'red','weight':'heavy','size':20},bbox=box_style)

ax.set_xlabel('1/L')
ax.set_ylabel('Rate Function')
fig.suptitle('Jxy = 1.0, hx(t) ramps from '+str(h0)+' to ' +str(hf)+',  $T=$'+str(t_f), fontsize=14, fontweight='bold')#+' \n $y=ax + b,  a = $'+"{:.2e}".format(a)+'$\u00B1$'+"{:.2e}".format(perr[0])+', \n$b=$'+"{:.2e}".format(b)+'\u00B1'+"{:.2e}".format(perr[1]), fontsize=14, fontweight='bold')
#ax.set_title('axes title')

plt.legend()
plt.show()
