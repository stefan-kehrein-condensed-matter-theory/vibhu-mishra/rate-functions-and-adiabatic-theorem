from quspin.operators import hamiltonian # Hamiltonians and operators
from quspin.basis import spin_basis_1d # Hilbert space spin basis
import numpy as np # generic math functions
from time import time
from quspin.tools.measurements import ent_entropy, diag_ensemble
import matplotlib.pyplot as plt
from scipy.integrate import complex_ode, odeint
import math
from scipy.optimize import curve_fit
from odeintw import odeintw
fig = plt.figure()
ax = fig.add_subplot()

# This code performs a linear curve fit for the log log plot for the rate function vs ramp time to obtain f(T) in the thermoynamic limit 

J = 1.0
chi = 1
h0 = 0.9   # Initial magnetic field
hf = 1.1   # Final magnetic field



n = 2
k0 = [float(0.0) for x in range(n)]   # The initial groundstate which will be time evolved
lf = [float(0.0) for x in range(n)]   # The final ground state whose overlap will be taken

l = 5

RF = [float(0.0) for x in range(l)]
T =  [float(0.0) for x in range(l)]


N = 20   # System size
# s = 25
s = 4
e = 2
#T0 = 20
for i in range(0,l):
    
    #Tf =  T0 + i/10 
    Tf = 2**(i+s)
    rate = (hf - h0)/Tf
    def h(t):
        return (h0 + rate*t)

    tStop = Tf
    tInc = 0.001
    t = np.arange(0., tStop, tInc)

    def f(y,t,q):
    #print(q)
    #print(len(y))
    #y = np.asarray(y)
    # unpack current values of y
    #Q, d, Omega = params  # unpack parameters
    #derivs = [omega,      # list of dy/dt=f functions -omega/Q + np.sin(theta) + d*np.cos(Omega*t)]
        derivs = [float(0.0) for x in range(2)]
    #print(kT[4])
    # derivs[0] =  (-1j)*( -2*(h(t) - J*np.cos(q))*y[0] - (1j)*(-2*chi*J*np.sin(q))*y[1] )
    # derivs[1] =  (-1j)*( +2*(h(t) - J*np.cos(q))*y[1] + (1j)*(-2*chi*J*np.sin(q))*y[0])
    
        derivs[0] =  2*(-1j)*( -(h(t) - J*np.cos(q))*y[0] - (1j)*(-chi*J*np.sin(q))*y[1] )
        derivs[1] =  2*(-1j)*( +(h(t) - J*np.cos(q))*y[1] + (1j)*(-chi*J*np.sin(q))*y[0])
    

    
    #print(len(derivs))
    
        return derivs

    print(Tf)

    for ni in range(1,N//2+1):
        #print(n)
        k = ((2*ni-1)*np.pi/N)
        #r=0

        epsilon_k0 = 2*J*np.sqrt((chi*np.sin(k))**2 + (np.cos(k) - h0/J)**2) 
        epsilon_kf = 2*J*np.sqrt((chi*np.sin(k))**2 + (np.cos(k) - hf/J)**2) 

        y_k0 = 2*chi*J*np.sin(k) + (0j)
        y_kf = 2*chi*J*np.sin(k) + (0j)

        z_k0 = 2*(h0 - J*np.cos(k)) 
        z_kf = 2*(hf - J*np.cos(k)) 

        u_k0 = (epsilon_k0 + z_k0)/np.sqrt(2*epsilon_k0*(epsilon_k0 + z_k0)) 
        u_kf = (epsilon_kf + z_kf)/np.sqrt(2*epsilon_kf*(epsilon_kf + z_kf)) 

        v_k0 = (1j)*y_k0/np.sqrt(2*epsilon_k0*(epsilon_k0 + z_k0))
        v_kf = (1j)*y_kf/np.sqrt(2*epsilon_kf*(epsilon_kf + z_kf))


        
        k0[0] = u_k0
        k0[1] = v_k0
        lf[0] = u_kf
        lf[1] = v_kf

        sol = odeintw(f,k0,t,args=(k,))
        kT = sol[-1,:]
        norm1 = np.linalg.norm(kT)
        #print(norm1)
        #print(norm1)
        #print(np.absolute(np.sum(np.conjugate(kT)*kT)))
        kT = kT/norm1
        #print(norm1)
        #print((1-np.absolute(np.sum(np.conjugate(kT)*lf)))*1000)
        #RF = RF - np.log(np.absolute(np.sum(np.conjugate(kT)*lf)))

        RF[i] = RF[i] - np.log(np.absolute(np.sum(np.conjugate(lf)*kT))**2)/(N)
        T[i] =  Tf#np.log2(Tf)
        
    #print(E0-E00)



def func(z,a,b):
    return a + b*z



values,errors = curve_fit(func,np.log2(T),np.log2(RF))

x = np.linspace(np.log2(e**s),np.log2(e**(l-1+s)),200)
c,d= values
#l1 = c/np.cbrt(x**2) 
l1 = c + x*d 


leg = ax.legend(prop={"size":30})

for label in (ax.get_xticklabels() + ax.get_yticklabels()):
	label.set_fontsize(20)



print(c,d)
print(errors)
plt.plot(x,l1,'-b',label = 'Line Fit')




# plt.plot(np.log2(T),np.log2(RF),'--ro',label='N = '+str(N))

plt.plot(np.log2(T),np.log2(RF),'--ro',label='N = '+str(N))

#leg = ax.legend(prop={"size":64})
plt.annotate('Slope = '+ str(round(d,4))+ '$\u00B1$' + str(round(errors[1][1],6)),xy = (5.5,-26), xytext=(4.5,-25),fontsize = 30,fontweight='bold')
ax.set_xlabel(r'$\log_2(T)$',fontsize = 30)
ax.set_ylabel(r'$\log_2(f(T))$',fontsize = 30)
fig.suptitle(r'$h$'+' ranges from '+str(h0)+' to '+str(hf), fontsize=30, fontweight='bold')
#ax.set_title('axes title')
plt.legend(prop={'size':30})
plt.show()