from quspin.operators import hamiltonian # Hamiltonians and operators
from quspin.basis import spin_basis_1d # Hilbert space spin basis
import numpy as np # generic math functions
from time import time
from quspin.tools.measurements import ent_entropy, diag_ensemble
import matplotlib.pyplot as plt
fig = plt.figure()
ax = fig.add_subplot()


# This code produces a plot of the rate function vs the ramp time for 3 different system sizes for the 
# transverse field Ising model with periodic boundary conditions.



hz=0.01*0 # z external field
J = 1.0 # xy interaction
Jzz_0 = 1.0 # zz interaction
vs=np.logspace(-2.0, 0.0, num=20, base=10)


h0 = 1.4
hf = 1.6
t0 = 1.0
f1 = 8
l = 30

RF =  [float(0.0) for x in range(l)]
T =  [float(0.0) for x in range(l)]
L = 14
print(L)
for k in range(0,l): 
    
    
    t_f = (k*f1)/l + t0
    rg = (hf - h0)/t_f

    def ramp_g(t):
        return (h0 + rg*t)
    ramp_args=[]


    basis = spin_basis_1d(L,zblock = +1,kblock = 0)
    #basis = spin_basis_1d(L,pauli=False)
# define operators with OBC using site-coupling lists
# site-coupling lists (PBC for both spin inversion sectors)
    h_field=[[-1,i] for i in range(L)]
    J_zz=[[-J,i,(i+1)%L] for i in range(L)] # PBC
    h_z=[[hz,i] for i in range(1)]
# static and dynamic lists
    static = [["z",h_z],["zz",J_zz]]
    dynamic =[["x",h_field,ramp_g,ramp_args]]
# compute the time-dependent Heisenberg Hamiltonian
    H_Ising = hamiltonian(static,dynamic,basis=basis,dtype=np.float64,check_symm = False)

    E0,psi_0=H_Ising.eigsh(time = 0.0,k=1,which = "SA",maxiter=1E6)
    

    E00,psi_0f=H_Ising.eigsh(time = t_f,k=1,which = "SA",maxiter=1E6)


    norm = np.linalg.norm(psi_0)
    psi_0 = psi_0/norm

    norm = np.linalg.norm(psi_0f)
    psi_0f = psi_0f/norm

    psi = H_Ising.evolve(psi_0, 0.0 ,t_f)
    norm = np.linalg.norm(psi)
    psi = psi/norm


    # norm2 = np.linalg.norm(psi_0f)
    # psi_0f = psi_0f/norm2
    RF[k] = -np.log(np.absolute(np.sum(np.conjugate(psi) * psi_0f))**2)/L
    T[k] = t_f

    #print(E0-E00)

plt.plot(T,RF,'-ro',label = 'L = '+ str(L))

RF =  [float(0.0) for x in range(l)]
T =  [float(0.0) for x in range(l)]
L = 16
print(L)
for k in range(0,l): 
    
    
    t_f = (k*f1)/l + t0
    rg = (hf - h0)/t_f

    def ramp_g(t):
        return (h0 + rg*t)
    ramp_args=[]


    basis = spin_basis_1d(L,zblock = +1,kblock = 0)
    #basis = spin_basis_1d(L,pauli=False)
# define operators with OBC using site-coupling lists
# site-coupling lists (PBC for both spin inversion sectors)
    h_field=[[-1,i] for i in range(L)]
    J_zz=[[-J,i,(i+1)%L] for i in range(L)] # PBC
    h_z=[[hz,i] for i in range(1)]
# static and dynamic lists
    static = [["z",h_z],["zz",J_zz]]
    dynamic =[["x",h_field,ramp_g,ramp_args]]
# compute the time-dependent Heisenberg Hamiltonian
    H_Ising = hamiltonian(static,dynamic,basis=basis,dtype=np.float64,check_symm = False)

    E0,psi_0=H_Ising.eigsh(time = 0.0,k=1,which = "SA",maxiter=1E6)
    

    E00,psi_0f=H_Ising.eigsh(time = t_f,k=1,which = "SA",maxiter=1E6)


    norm = np.linalg.norm(psi_0)
    psi_0 = psi_0/norm

    norm = np.linalg.norm(psi_0f)
    psi_0f = psi_0f/norm

    psi = H_Ising.evolve(psi_0, 0.0 ,t_f)
    norm = np.linalg.norm(psi)
    psi = psi/norm


    # norm2 = np.linalg.norm(psi_0f)
    # psi_0f = psi_0f/norm2
    RF[k] = -np.log(np.absolute(np.sum(np.conjugate(psi) * psi_0f))**2)/L
    T[k] = t_f

    #print(E0-E00)

plt.plot(T,RF,'-bo',label = 'L = '+ str(L))

RF =  [float(0.0) for x in range(l)]
T =  [float(0.0) for x in range(l)]
L = 18
print(L)
for k in range(0,l): 
    
    
    t_f = (k*f1)/l + t0
    rg = (hf - h0)/t_f

    def ramp_g(t):
        return (h0 + rg*t)
    ramp_args=[]


    basis = spin_basis_1d(L,zblock = +1,kblock = 0)
    #basis = spin_basis_1d(L,pauli=False)
# define operators with OBC using site-coupling lists
# site-coupling lists (PBC for both spin inversion sectors)
    h_field=[[-1,i] for i in range(L)]
    J_zz=[[-J,i,(i+1)%L] for i in range(L)] # PBC
    h_z=[[hz,i] for i in range(1)]
# static and dynamic lists
    static = [["z",h_z],["zz",J_zz]]
    dynamic =[["x",h_field,ramp_g,ramp_args]]
# compute the time-dependent Heisenberg Hamiltonian
    H_Ising = hamiltonian(static,dynamic,basis=basis,dtype=np.float64,check_symm = False)

    E0,psi_0=H_Ising.eigsh(time = 0.0,k=1,which = "SA",maxiter=1E6)
    

    E00,psi_0f=H_Ising.eigsh(time = t_f,k=1,which = "SA",maxiter=1E6)


    norm = np.linalg.norm(psi_0)
    psi_0 = psi_0/norm

    norm = np.linalg.norm(psi_0f)
    psi_0f = psi_0f/norm

    psi = H_Ising.evolve(psi_0, 0.0 ,t_f)
    norm = np.linalg.norm(psi)
    psi = psi/norm


    # norm2 = np.linalg.norm(psi_0f)
    # psi_0f = psi_0f/norm2
    RF[k] = -np.log(np.absolute(np.sum(np.conjugate(psi) * psi_0f))**2)/L
    T[k] = t_f

    #print(E0-E00)

plt.plot(T,RF,'-go',label = 'L = '+ str(L))
for label in (ax.get_xticklabels() + ax.get_yticklabels()):
	label.set_fontsize(20)


ax.set_xlabel(r'$T$',fontsize = 30)
ax.set_ylabel(r'$f(T)$',fontsize = 30)
fig.suptitle(r'$h$'+' ranges from '+str(h0)+' to '+str(hf), fontsize=30, fontweight='bold')
#ax.set_title('axes title')
plt.legend(prop={'size':30})
plt.show()