from zlib import Z_BLOCK
from quspin.operators import hamiltonian # Hamiltonians and operators
from quspin.basis import spin_basis_1d # Hilbert space spin basis
import numpy as np # generic math functions
from time import time
from quspin.tools.measurements import ent_entropy, diag_ensemble
import matplotlib.pyplot as plt
from scipy.integrate import complex_ode, odeint
import math
from odeintw import odeintw
fig = plt.figure()
#ax = fig.add_subplot()


# This code is just the combination of the codes for QuSpin, Time evolved Bogoliubov coefficients and Instantaneous Eigenstate framework
# to see if they yield the same result for the rate function or not. Cannot look at systems larger than size ~20 due to QuSpin.



J = 1.0
chi = 1
h0 = 1.4
hf = 1.6
l = 80
f1 = 15
t0 = 1

n = 2
k0 = [float(0.0) for x in range(n)]   # The initial groundstate which will be time evolved
# lf = [float(0.0) for x in range(n)]   # The final ground state whose overlap will be taken


RF =  [float(0.0) for x in range(l)]
T =  [float(0.0) for x in range(l)]

N = 10

for i in range(0,l):
    
    Tf = (i*f1)/l + t0
    rate = (hf - h0)/Tf
    def h(t):
        return (h0 + rate*t)

    tStop = Tf
    tInc = 0.001
    t = np.arange(0., tStop, tInc)

    def f(y,t,q):
    #print(q)
    #print(len(y))
    #y = np.asarray(y)
    # unpack current values of y
    #Q, d, Omega = params  # unpack parameters
    #derivs = [omega,      # list of dy/dt=f functions -omega/Q + np.sin(theta) + d*np.cos(Omega*t)]
        derivs = [float(0.0) for x in range(2)]

        phi_10 = 2*((np.sin(q)**2)*np.log(((h(t) - np.cos(q)) + np.sqrt(np.sin(q)**2 + (h(t) - np.cos(q))**2))/((h0 - np.cos(q)) + np.sqrt(np.sin(q)**2 + (h0 - np.cos(q))**2))) + (h(t) - np.cos(q))*np.sqrt(np.sin(q)**2 + (h(t) - np.cos(q))**2)  - (h0 - np.cos(q))*np.sqrt(np.sin(q)**2 + (h0 - np.cos(q))**2))/rate 
    #print(kT[4])
    # derivs[0] =  (-1j)*( -2*(h(t) - J*np.cos(q))*y[0] - (1j)*(-2*chi*J*np.sin(q))*y[1] )
    # derivs[1] =  (-1j)*( +2*(h(t) - J*np.cos(q))*y[1] + (1j)*(-2*chi*J*np.sin(q))*y[0])
        #phi_10 = 0
        derivs[0] =  -y[1]*np.exp(-1j*phi_10)*rate*(-1j*2*np.sin(q))/(4*(np.sin(q)**2 + (h(t) - np.cos(q))**2)) 
        derivs[1] =  -y[0]*np.exp(+1j*phi_10)*rate*(-1j*2*np.sin(q))/(4*(np.sin(q)**2 + (h(t) - np.cos(q))**2))
    

    
    #print(len(derivs))
    
        return derivs

    print(Tf)

    for ni in range(1,N//2+1):
        #print(n)
        k = ((2*ni-1)*np.pi/N)
        #r=0

        epsilon_k0 = 2*J*np.sqrt((chi*np.sin(k))**2 + (np.cos(k) - h0/J)**2) 
        epsilon_kf = 2*J*np.sqrt((chi*np.sin(k))**2 + (np.cos(k) - hf/J)**2) 

        y_k0 = 2*chi*J*np.sin(k) + (0j)
        y_kf = 2*chi*J*np.sin(k) + (0j)

        z_k0 = 2*(h0 - J*np.cos(k)) 
        z_kf = 2*(hf - J*np.cos(k)) 

        u_k0 = (epsilon_k0 + z_k0)/np.sqrt(2*epsilon_k0*(epsilon_k0 + z_k0)) 
        u_kf = (epsilon_kf + z_kf)/np.sqrt(2*epsilon_kf*(epsilon_kf + z_kf)) 

        v_k0 = (1j)*y_k0/np.sqrt(2*epsilon_k0*(epsilon_k0 + z_k0))
        v_kf = (1j)*y_kf/np.sqrt(2*epsilon_kf*(epsilon_kf + z_kf))


        
        k0[0] = 1 + (0j)
        k0[1] = 0 + (0j)
        sol = odeintw(f,k0,t,args=(k,))
        kT = sol[-1,:]
        norm1 = np.linalg.norm(kT)
        #print(norm1)
        #print(norm1)
        #print(np.absolute(np.sum(np.conjugate(kT)*kT)))
        kT = kT/norm1
        #print(norm1)
        #print(np.absolute(np.sum(np.conjugate(kT)*lf)))
        #print(np.absolute(kT[0]))
        #print(np.absolute(kT[1]))
        RF[i] = RF[i] - np.log(np.absolute(kT[0])**2)/N

        T[i] = (Tf) 
  #      print(np.absolute(np.sum(np.conjugate(lf)*kT)))
    #print(E0-E00)









n = 2
k0 = [float(0.0) for x in range(n)]   # The initial groundstate which will be time evolved
lf = [float(0.0) for x in range(n)]   # The final ground state whose overlap will be taken


RF =  [float(0.0) for x in range(l)]
T =  [float(0.0) for x in range(l)]




for i in range(0,l):
    
    Tf = (i*f1)/l + t0
    rate = (hf - h0)/Tf
    def h(t):
        return (h0 + rate*t)

    tStop = Tf
    
    t = np.arange(0., tStop, tInc)

    def f(y,t,q):
    #print(q)
    #print(len(y))
    #y = np.asarray(y)
    # unpack current values of y
    #Q, d, Omega = params  # unpack parameters
    #derivs = [omega,      # list of dy/dt=f functions -omega/Q + np.sin(theta) + d*np.cos(Omega*t)]
        derivs = [float(0.0) for x in range(2)]
    #print(kT[4])
    # derivs[0] =  (-1j)*( -2*(h(t) - J*np.cos(q))*y[0] - (1j)*(-2*chi*J*np.sin(q))*y[1] )
    # derivs[1] =  (-1j)*( +2*(h(t) - J*np.cos(q))*y[1] + (1j)*(-2*chi*J*np.sin(q))*y[0])
    
        derivs[0] =  2*(-1j)*( -(h(t) - J*np.cos(q))*y[0] - (1j)*(-chi*J*np.sin(q))*y[1] )
        derivs[1] =  2*(-1j)*( +(h(t) - J*np.cos(q))*y[1] + (1j)*(-chi*J*np.sin(q))*y[0])
    

    
    #print(len(derivs))
    
        return derivs

    #print(Tf)

    for ni in range(1,N//2+1):
        #print(n)
        k = ((2*ni-1)*np.pi/N)
        #r=0

        epsilon_k0 = 2*J*np.sqrt((chi*np.sin(k))**2 + (np.cos(k) - h0/J)**2) 
        epsilon_kf = 2*J*np.sqrt((chi*np.sin(k))**2 + (np.cos(k) - hf/J)**2) 

        y_k0 = 2*chi*J*np.sin(k) + (0j)
        y_kf = 2*chi*J*np.sin(k) + (0j)

        z_k0 = 2*(h0 - J*np.cos(k)) 
        z_kf = 2*(hf - J*np.cos(k)) 

        u_k0 = (epsilon_k0 + z_k0)/np.sqrt(2*epsilon_k0*(epsilon_k0 + z_k0)) 
        u_kf = (epsilon_kf + z_kf)/np.sqrt(2*epsilon_kf*(epsilon_kf + z_kf)) 

        v_k0 = (1j)*y_k0/np.sqrt(2*epsilon_k0*(epsilon_k0 + z_k0))
        v_kf = (1j)*y_kf/np.sqrt(2*epsilon_kf*(epsilon_kf + z_kf))


        
        k0[0] = u_k0
        k0[1] = v_k0
        lf[0] = u_kf
        lf[1] = v_kf

        sol = odeintw(f,k0,t,args=(k,))
        kT = sol[-1,:]
        norm1 = np.linalg.norm(kT)
        #print(norm1)
        #print(norm1)
        #print(np.absolute(np.sum(np.conjugate(kT)*kT)))
        kT = kT/norm1
        #print(norm1)
        #print(np.absolute(np.sum(np.conjugate(kT)*lf)))
        #RF = RF - np.log(np.absolute(np.sum(np.conjugate(kT)*lf)))

        RF[i] = RF[i] - np.log(np.absolute(np.sum(np.conjugate(lf)*kT))**2)/N
        T[i] = (Tf) 
        #print(np.absolute(np.sum(np.conjugate(lf)*kT)))
    #print(E0-E00)








 # z external field
J = 1.0 # xy interaction
Jzz_0 = 1.0 # zz interaction

hz = 0.01
vs=np.logspace(-2.0, 0.0, num=20, base=10)



RF2 =  [float(0.0) for x in range(l)]
T2 =  [float(0.0) for x in range(l)]

for i in range(0,l): 
    
    
    t_f = (i*f1)/l + t0
    rg = (hf - h0)/t_f
    def ramp_g(t):
        return (h0 + rg*t)
    ramp_args=[]


    #basis = spin_basis_1d(N,zblock = +1)
    basis = spin_basis_1d(N,zblock = +1,pblock = 1,kblock = 0)
# define operators with OBC using site-coupling lists
# site-coupling lists (PBC for both spin inversion sectors)
    h_field=[[-1,i] for i in range(N)]
    J_zz=[[-J,i,(i+1)%N] for i in range(N)] # PBC
    h_z=[[hz,i] for i in range(1)]
# static and dynamic lists
    static = [["z",h_z],["zz",J_zz]]
    dynamic =[["x",h_field,ramp_g,ramp_args]]
# compute the time-dependent Heisenberg Hamiltonian
    H_Ising = hamiltonian(static,dynamic,basis=basis,dtype=np.float64,check_symm = False)

    E0,psi_0=H_Ising.eigsh(time = 0.0,k=1,which = "SA",maxiter=1E6)
    

    E00,psi_0f=H_Ising.eigsh(time = t_f,k=1,which = "SA",maxiter=1E6)


    norm = np.linalg.norm(psi_0)
    psi_0 = psi_0/norm

    norm = np.linalg.norm(psi_0f)
    psi_0f = psi_0f/norm

    psi = H_Ising.evolve(psi_0, 0.0 ,t_f)
    norm = np.linalg.norm(psi)
    psi = psi/norm


    # norm2 = np.linalg.norm(psi_0f)
    # psi_0f = psi_0f/norm2
    RF2[i] = -np.log(np.absolute(np.sum(np.conjugate(psi)*psi_0f))**2)/N
    T2[i] =  (t_f) 

    #print(E0-E00)


plt.figure(dpi=250)

# plt.plot(T,RF,'-rx',label=' Instantaneous Eigenstates')

# plt.plot(T,RF,'-b+',label=' Bogoliubov Coefficients')

plt.plot(T2,(RF2),'-yo',label='Quspin')






# for label in (ax.get_xticklabels() + ax.get_yticklabels()):
# 	label.set_fontsize(200)









plt.annotate(r'$T_D$', xy=(+1.2, 0.00029), xytext=(+4.2, 0.00029),fontsize = 30, arrowprops=dict(facecolor='black', shrink=0.05))
plt.annotate(r'$T_C$ ', xy=(2.5, 0.00001), xytext=(4, 0.00015),fontsize = 30, arrowprops=dict(facecolor='black', shrink=0.05))
#plt.annotate('Unique \n'+r' F.M G.S $<m_z> = 0$', xy=(+1.7, -6.25), xytext=(+1.0, -9.0),fontsize = 20, arrowprops=dict(facecolor='black', shrink=0.05))
#plt.annotate('A.F.M G.S', xy=(0.5, -5.66), xytext=(1.75, -5.66),fontsize = 20, arrowprops=dict(facecolor='black', shrink=0.05))
plt.annotate(r'$T_A$', xy=(8, 0.000005), xytext=(8, 0.0002),fontsize = 30, arrowprops=dict(facecolor='black', shrink=0.05))

#plt.xticks(fontsize=14, rotation=90)
plt.xticks([])
plt.yticks([])



plt.xlabel(r'$T$',fontsize = 20)
plt.ylabel(r'$f(T)$',fontsize = 20)
#plt.suptitle(r'$h$'+' ramps from '+str(h0)+' to '+str(hf)+ '\n L = '+ str(N), fontsize=30, fontweight='bold')
#ax.set_title('axes title')
#plt.legend(prop={'size':30})




plt.show()