from quspin.operators import hamiltonian # Hamiltonians and operators
from quspin.basis import spin_basis_1d # Hilbert space spin basis
import numpy as np # generic math functions
from time import time
from quspin.tools.measurements import ent_entropy, diag_ensemble
import matplotlib.pyplot as plt
from scipy.integrate import complex_ode, odeint
import math
from scipy.optimize import curve_fit
from odeintw import odeintw
fig = plt.figure()
ax = fig.add_subplot()


# This code gives the rate functions for Luttinger Liquid, i.e XXZ model in the XY phase.





l = 6
Jz0 = -0.1
Jzf = +0.1
Jxy = +1.0*-1

# kappa0 = 1 + (np.pi/2)*(Jxy/Jz0)
# kappa0 = 1 + (np.pi/2)*(Jxy/Jzf)
# alpha0 = (np.pi/2)*(kappa0 + 1 - np.sqrt())
# alphaf = (np.pi/2)

def alpha(J):

    kappa = (np.pi/2)*(Jxy/J)
    if J<0:
        return (kappa + 1 - np.sqrt(kappa*(kappa+2)))*0.5
    if J>0:
        return (kappa + 1 + np.sqrt(kappa*(kappa+2)))*0.5
    if J==0:
        return 0
vF = Jxy # Fermi Velocity
omega = vF



Normalization0 = np.sqrt(np.sqrt(1-4*alpha(Jz0)**2))
Normalizationf = np.sqrt(np.sqrt(1-4*alpha(Jzf)**2))

RF =  [float(0.0) for x in range(l)]
T =  [float(0.0) for x in range(l)]

N = 1000   # Truncated Syatem size
n = 8   # No. of terms from the wavefunction we consider in the problem


# Make time array for solution

k0 = [float(0.0) for x in range(n)]   # The initial groundstate which will be time evolved
lf = [float(0.0) for x in range(n)]   # The final ground state whose overlap will be taken

for i in range(0,n):
    k0[i] = (((-alpha(Jz0))**i)*np.sqrt(math.factorial(2*i))/math.factorial(i))*Normalization0 + (0j)
    lf[i] = (((-alpha(Jzf))**i)*np.sqrt(math.factorial(2*i))/math.factorial(i))*Normalizationf + (0j)




# sol = odeintw(f,k0,t)  #complex_ode(f)




# kT = sol[-1,:]
# norm1 = np.linalg.norm(kT)
# kT = kT/norm1

# O = (np.absolute(np.sum(np.conjugate(kT)*lf)))**2
# print(O)

for i in range(0,l):

    Tf = 2**(i)
    tStop = Tf
    tInc = 0.001
    t = np.arange(0., tStop, tInc)

    rate = (Jzf-Jz0)/Tf

    print(Tf)
    def delta(t):
        return (Jz0 + rate*t)*2/np.pi     # The 2pi is from the difference between Giamarchi and Cazalilla


    def f(y,t,q):
    #print(q)
    #print(len(y))
    #y = np.asarray(y)
    # unpack current values of y
    #Q, d, Omega = params  # unpack parameters
    #derivs = [omega,      # list of dy/dt=f functions -omega/Q + np.sin(theta) + d*np.cos(Omega*t)]
        derivs = [float(0.0) for x in range(n)]
        #print(kT[4])
        derivs[0] =  (-1j)*q*(delta(t)/2)*(np.sqrt(2))*y[1]
        derivs[n-1] =  (-1j)*q*(delta(t)/2)*(np.sqrt((2*(n-1))*(2*(n-1)-1)))*y[n-2] + (-1j)*q*(omega + delta(t))*(2*(n-1))*y[n-1]
        for i in range(1,n-1):
            derivs[i] =  (-1j)*q*(delta(t)/2)*(np.sqrt((2*i)*(2*i-1)))*y[i-1] + (-1j)*q*(omega + delta(t))*(2*i)*y[i] + (-1j)*q*(delta(t)/2)*(np.sqrt((2*i+2)*(2*i+1)))*y[i+1]


        
        #print(len(derivs))
        
        return derivs


    for ni in range(1,N//2+1):
#print(n)
        q = ((2*np.pi/N))*ni
        #r=0
        sol = odeintw(f,k0,t,args=(q,))
        kT = sol[-1,:]
        norm1 = np.linalg.norm(kT)
        #print(norm1)
        #print(np.absolute(np.sum(np.conjugate(kT)*kT)))
        kT = kT/norm1
        RF[i] = RF[i] - np.log(np.absolute(np.sum(np.conjugate(kT)*lf))**2)*2/N  
        T[i] = Tf #np.log2(Tf)



  # The 2 is to account for the doublng of the Hilbert space


def func(z,a,b):
    return a + b*z

leg = ax.legend(prop={"size":30})

for label in (ax.get_xticklabels() + ax.get_yticklabels()):
	label.set_fontsize(16)


values,errors = curve_fit(func,np.log2(T),np.log2(RF))

x = np.linspace(np.log2(2**0),np.log2(2**(l-1)),200)
c,d= values
#l1 = c/np.cbrt(x**2) 
l1 = c + x*d 

print(c,d)
print(errors)
plt.plot(x,l1,'-b',label = 'Line Fit')




plt.plot(np.log2(T),np.log2(RF),'--ro',label = 'Rate Function')

plt.legend(prop={'size':30})
ax.set_xlabel(r'$\log_2(T)$',fontsize = 30)
ax.set_ylabel(r'$\log_2(f(T))$',fontsize = 30)
plt.annotate('Slope = '+ str(round(d,4))+ '$\u00B1$' + str(round(errors[1][1],5)),xy = (5.5,-26), xytext=(1,-14),fontsize = 30,fontweight='bold')
fig.suptitle(r'$J_z$'+' ranges from '+str(Jz0)+' to '+str(Jzf), fontsize=30, fontweight='bold')
#ax.set_title('axes title')

plt.show()






















#print(-np.log(O))
# print(np.absolute(np.sum(np.conjugate(kT)*kT)))
# print(np.absolute(np.sum(np.conjugate(lf)*lf)))
# print(np.absolute(np.sum(np.conjugate(k0)*k0)))


# print(k0)
# print(lf)

# print(Normalization0)
# print(Normalizationf)
# print(alpha(Jz0))
# print(alpha(Jzf))
















# #
# ##### define model parameters #####
# L = 10 # system size
# hz=0.01 # z external field
# Jxy = 1.0 # xy interaction
# Jzz_0 = -0.5 # zz interaction
# Jzz_f = 2.5
# vs=np.logspace(-2.0, 0.0, num=20, base=10)


# global v # declare ramp speed v a global variable
# ##### set up Heisenberg Hamiltonian with linearly varying zz-interaction #####
# # define linear ramp function
# # v = 1/2/2/2/2/2/2/2 # declare ramp speed variable
# # def ramp(t):
# #     return (+0.5 + v*t)
# # ramp_args=[]

# #ti = time() # get start time



# # compute basis in the 0-total magnetisation sector (requires L even)
# # basis = spin_basis_1d(L,pauli=False)
# # # define operators with OBC using site-coupling lists
# # J_zz = [[Jzz_0,i,i+1] for i in range(L-1)] # OBC
# # J_xy = [[Jxy/2.0,i,i+1] for i in range(L-1)] # OBC
# # # static and dynamic lists
# # static = [["+-",J_xy],["-+",J_xy]]
# # dynamic =[["zz",J_zz,ramp,ramp_args]]
# # # compute the time-dependent Heisenberg Hamiltonian
# # H_XXZ = hamiltonian(static,dynamic,basis=basis,dtype=np.float64,check_symm = False)




# # ##### various exact diagonalisation routines #####
# # # calculate entire spectrum only
# # #E=H_XXZ.eigvalsh()
# # # calculate full eigensystem
# # #E,V=H_XXZ.eigh()
# # # calculate minimum and maximum energy only
# # Emin0,Emax0=H_XXZ.eigsh(time=0.0, k=2,which="BE",maxiter=1E4,return_eigenvectors=False)
# # # calculate the eigenstate closest to energy E_star

# # E0,psi_0=H_XXZ.eigsh(time = 0.0,k=1,sigma=Emin0-1,maxiter=1E4)
# # #psi_0 = psi_0.reshape((-1,))
# # norm = np.linalg.norm(psi_0)
# # psi_0 = psi_0/norm

# # # determine total ramp time
# # t_f = 2.0/v
# # # time-evolve state from time 0. 0 to time t_f
# # psi = H_XXZ.evolve(psi_0, 0.0 ,t_f)
# # norm1 = np.linalg.norm(psi)
# # psi = psi/norm1
# # EminF,EmaxF=H_XXZ.eigsh(time=t_f, k=2,which="BE",maxiter=1E4,return_eigenvectors=False)
# # # calculate the eigenstate closest to energy E_star

# # E00,psi_0f=H_XXZ.eigsh(time = t_f,k=1,sigma=EminF-1,maxiter=1E4)
# # #psi_0f = psi_0.reshape((-1,))



# # norm2 = np.linalg.norm(psi_0f)
# # psi_0f = psi_0f/norm2
# # # print(psi)
# # # print(psi_0f)
# # print(-np.log(np.absolute(np.sum(psi.conjugate() * psi_0f)))/L)
# l=10

# t_f = 2**l
# # v = 4/t_f



# O =  [float(0.0) for x in range(t_f)]
# #EEGST =  [float(0.0) for x in range(t_f)]
# T =  [float(0.0) for x in range(t_f)]

# # compute basis in the 0-total magnetisation sector (requires L even)
# basis = spin_basis_1d(L,pauli=False)
# # define operators with OBC using site-coupling lists
# J_zz0 = [[Jzz_0,i,i+1] for i in range(L-1)] # OBC
# J_zzf = [[Jzz_f,i,i+1] for i in range(L-1)] # OBC

# J_xy = [[Jxy/2.0,i,i+1] for i in range(L-1)] # OBC
# h_z=[[hz,i] for i in range(1)]
# # static and dynamic lists


# static0 = [["+-",J_xy],["-+",J_xy],["z",h_z],["zz",J_zz0]]
# staticf = [["+-",J_xy],["-+",J_xy],["z",h_z],["zz",J_zzf]]
# dynamic =[]
# # compute the time-dependent Heisenberg Hamiltonian
# H_XXZ0 = hamiltonian(static0,dynamic,basis=basis,dtype=np.float64,check_symm = False)
# H_XXZf = hamiltonian(staticf,dynamic,basis=basis,dtype=np.float64,check_symm = False)
# Emin0,Emax0=H_XXZ0.eigsh(time=0.0, k=2,which="BE",maxiter=1E4,return_eigenvectors=False)
# # calculate the eigenstate closest to energy E_star

# E0,psi_0=H_XXZ0.eigsh(time = 0.0,k=1,sigma=Emin0-1,maxiter=1E4)
# #psi_0 = psi_0.reshape((-1,))
# norm = np.linalg.norm(psi_0)
# psi_0 = psi_0/norm

# psi = psi_0

# psi = H_XXZf.evolve(psi, 0 ,10)
# norm1 = np.linalg.norm(psi)
# psi = psi/norm1
# EminF,EmaxF=H_XXZf.eigsh(time=0.0, k=2,which="BE",maxiter=1E4,return_eigenvectors=False)
# # calculate the eigenstate closest to energy E_star

# E00,psi_0f=H_XXZf.eigsh(time = 0.0,k=1,sigma=EminF-1,maxiter=1E4)
# #psi_0f = psi_0.reshape((-1,))



# norm2 = np.linalg.norm(psi_0f)
# psi_0f = psi_0f/norm2

# O1 = (np.absolute(np.sum(psi_0f.conjugate() * psi_0)))
# O2 = (np.absolute(np.sum(psi_0f.conjugate() * psi)))

# print(O1,O2)

# #for i in range(0,t_f): 
    
    
# #ti = time() # get start time




# #     psi = H_XXZf.evolve(psi, i ,i+1)
# #     norm1 = np.linalg.norm(psi)
# #     psi = psi/norm1
# #     EminF,EmaxF=H_XXZf.eigsh(time=0.0, k=2,which="BE",maxiter=1E4,return_eigenvectors=False)
# # # calculate the eigenstate closest to energy E_star

# #     E00,psi_0f=H_XXZf.eigsh(time = 0.0,k=1,sigma=EminF-1,maxiter=1E4)
# # #psi_0f = psi_0.reshape((-1,))



# #     norm2 = np.linalg.norm(psi_0f)
# #     psi_0f = psi_0f/norm2
#     #subsys = range(basis.L//2) # define subsystem
    
#     # O[i] = (np.absolute(np.sum(psi_0f.conjugate() * psi)))
#     # # EET[i] = ent_entropy(psi,basis,chain_subsys=subsys)["Sent"]
#     # # EEGST[i] = ent_entropy(psi_0f,basis,chain_subsys=subsys)["Sent"]
#     # T[i] = i
#     # calculate entanglement entropy
    






# # plt.plot(T,O,'--ro',label = 'Overlap of time evolved G.S')
# # #plt.plot(T,EEGST,'--bo',label = 'E.E of instantaneous G.S')
# # ax.set_xlabel('Time')
# # ax.set_ylabel('Bipartite E.E')
# # fig.suptitle('L=10, Jxy = 1.0, Jz goes from -0.5 to -1.5, h_z=0.01, T = 1024', fontsize=14, fontweight='bold')
# # #ax.set_title('axes title')
# # plt.legend()
# # plt.show()
