from quspin.operators import hamiltonian # Hamiltonians and operators
from quspin.basis import spin_basis_1d # Hilbert space spin basis
import numpy as np # generic math functions
from time import time
from quspin.tools.measurements import ent_entropy, diag_ensemble
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
fig = plt.figure()
ax = fig.add_subplot()


# This code gives the rate functions f(T) vs T for the XXZ model for 4 different system sizes.


hz=0.01*0 # z external field
Jxy = -1.0 # xy interaction
Jzz_0 = -1.0 # zz interaction
vs=np.logspace(-2.0, 0.0, num=20, base=10)


Jz0 = -4.4
Jzf = -4.6

# T close to L regime
l = 40
f1 = 10

# l = 15
# s = 10

RF1 =  [float(0.0) for x in range(l)]
RF2 =  [float(0.0) for x in range(l)]
RF3 =  [float(0.0) for x in range(l)]
RF4 =  [float(0.0) for x in range(l)]

T =  [float(0.0) for x in range(l)]
t0 = 2

L = 12
print(L)
for k in range(0,l): 
    
    
    t_f = t0 + k*f1/l
    rg = (Jzf - Jz0)/t_f

    def ramp_g(t):
        return (Jz0 + rg*t)
    ramp_args=[]

    #basis = spin_basis_1d(L,pauli=False)
    basis = spin_basis_1d(L,pauli=False,zblock = +1,pblock = +1,kblock = 0)
    #basis = spin_basis_1d(L,pauli=False,Nup=L//2,zblock = +1,pblock = +1,kblock = 0)
    
    J_zz = [[Jzz_0,i,(i+1)%L] for i in range(L)] # PBC
    J_xy = [[Jxy/2.0,i,(i+1)%L] for i in range(L)] # PBC
    # J_zz = [[Jzz_0,i,i+1] for i in range(L-1)] # OBC
    # J_xy = [[Jxy/2.0,i,i+1] for i in range(L-1)] # OBC
    h_z=[[-hz*2,i] for i in range(L)]
# static and dynamic lists
    static = [["+-",J_xy],["-+",J_xy],["z",h_z]]
    static2 = [["+-",J_xy],["-+",J_xy]]
    dynamic =[["zz",J_zz,ramp_g,ramp_args]]
# compute the time-dependent Heisenberg Hamiltonian
    H_XXZ = hamiltonian(static,dynamic,basis=basis,dtype=np.float64,check_symm = False)
    H_XXZ_D = hamiltonian(static2,dynamic,basis=basis,dtype=np.float64,check_symm = False)
    E0,psi_0=H_XXZ.eigsh(time = 0.0,k=1,which = "SA",maxiter=1E6)
    

    E00,psi_0f=H_XXZ.eigsh(time = t_f,k=1,which = "SA",maxiter=1E6)



    norm = np.linalg.norm(psi_0)
    psi_0 = psi_0/norm

    norm = np.linalg.norm(psi_0f)
    psi_0f = psi_0f/norm

    psi = H_XXZ_D.evolve(psi_0, 0.0 ,t_f)
    norm = np.linalg.norm(psi)
    psi = psi/norm


    # norm2 = np.linalg.norm(psi_0f)
    # psi_0f = psi_0f/norm2
    RF1[k] = -np.log(np.absolute(np.sum(np.conjugate(psi) * psi_0f))**2)/L
    T[k] = (t_f)

    #print(E0-E00)

L = 14
print(L)
for k in range(0,l): 
    
    
    t_f = t0 + k*f1/l
    rg = (Jzf - Jz0)/t_f

    def ramp_g(t):
        return (Jz0 + rg*t)
    ramp_args=[]

    #basis = spin_basis_1d(L,pauli=False)
    basis = spin_basis_1d(L,pauli=False,zblock = +1,pblock = +1,kblock = 0)
    #basis = spin_basis_1d(L,pauli=False,Nup=L//2,zblock = +1,pblock = +1,kblock = 0)
    
    J_zz = [[Jzz_0,i,(i+1)%L] for i in range(L)] # PBC
    J_xy = [[Jxy/2.0,i,(i+1)%L] for i in range(L)] # PBC
    # J_zz = [[Jzz_0,i,i+1] for i in range(L-1)] # OBC
    # J_xy = [[Jxy/2.0,i,i+1] for i in range(L-1)] # OBC
    h_z=[[-hz*2,i] for i in range(L)]
# static and dynamic lists
    static = [["+-",J_xy],["-+",J_xy],["z",h_z]]
    static2 = [["+-",J_xy],["-+",J_xy]]
    dynamic =[["zz",J_zz,ramp_g,ramp_args]]
# compute the time-dependent Heisenberg Hamiltonian
    H_XXZ = hamiltonian(static,dynamic,basis=basis,dtype=np.float64,check_symm = False)
    H_XXZ_D = hamiltonian(static2,dynamic,basis=basis,dtype=np.float64,check_symm = False)
    
    E0,psi_0=H_XXZ.eigsh(time = 0.0,k=1,which = "SA",maxiter=1E6)
    #EminF,EmaxF=H_XXZ.eigsh(time=t_f, k=2,which="BE",maxiter=1E4,return_eigenvectors=False,)
    # calculate the eigenstate closest to energy E_star

    E00,psi_0f=H_XXZ.eigsh(time = t_f,k=1,which = "SA",maxiter=1E6)



    norm = np.linalg.norm(psi_0)
    psi_0 = psi_0/norm

    norm = np.linalg.norm(psi_0f)
    psi_0f = psi_0f/norm

    psi = H_XXZ_D.evolve(psi_0, 0.0 ,t_f)
    norm = np.linalg.norm(psi)
    psi = psi/norm


    # norm2 = np.linalg.norm(psi_0f)
    # psi_0f = psi_0f/norm2
    RF2[k] = -np.log(np.absolute(np.sum(np.conjugate(psi) * psi_0f))**2)/L
    T[k] = (t_f)

    #print(E0-E00)

L = 16
print(L)
for k in range(0,l): 
    
    
    t_f = t0 + k*f1/l
    rg = (Jzf - Jz0)/t_f

    def ramp_g(t):
        return (Jz0 + rg*t)
    ramp_args=[]

    #basis = spin_basis_1d(L,pauli=False)
    basis = spin_basis_1d(L,pauli=False,zblock = +1,pblock = +1,kblock = 0)
    #basis = spin_basis_1d(L,pauli=False,Nup=L//2,zblock = +1,pblock = +1,kblock = 0)
    
    J_zz = [[Jzz_0,i,(i+1)%L] for i in range(L)] # PBC
    J_xy = [[Jxy/2.0,i,(i+1)%L] for i in range(L)] # PBC
    # J_zz = [[Jzz_0,i,i+1] for i in range(L-1)] # OBC
    # J_xy = [[Jxy/2.0,i,i+1] for i in range(L-1)] # OBC
    h_z=[[-hz*2,i] for i in range(L)]
# static and dynamic lists
    static = [["+-",J_xy],["-+",J_xy],["z",h_z]]
    static2 = [["+-",J_xy],["-+",J_xy]]
    dynamic =[["zz",J_zz,ramp_g,ramp_args]]
# compute the time-dependent Heisenberg Hamiltonian
    H_XXZ = hamiltonian(static,dynamic,basis=basis,dtype=np.float64,check_symm = False)
    H_XXZ_D = hamiltonian(static2,dynamic,basis=basis,dtype=np.float64,check_symm = False)
    #Emin0,Emax0=H_XXZ.eigsh(time=0.0, k=2,which="BE",maxiter=1E4,return_eigenvectors=False)
# calculate the eigenstate closest to energy E_star

    E0,psi_0=H_XXZ.eigsh(time = 0.0,k=1,which = "SA",maxiter=1E6)
    #EminF,EmaxF=H_XXZ.eigsh(time=t_f, k=2,which="BE",maxiter=1E4,return_eigenvectors=False,)
# calculate the eigenstate closest to energy E_star

    E00,psi_0f=H_XXZ.eigsh(time = t_f,k=1,which = "SA",maxiter=1E6)




    norm = np.linalg.norm(psi_0)
    psi_0 = psi_0/norm

    norm = np.linalg.norm(psi_0f)
    psi_0f = psi_0f/norm

    psi = H_XXZ_D.evolve(psi_0, 0.0 ,t_f)
    norm = np.linalg.norm(psi)
    psi = psi/norm


    # norm2 = np.linalg.norm(psi_0f)
    # psi_0f = psi_0f/norm2
    RF3[k] = -np.log(np.absolute(np.sum(np.conjugate(psi) * psi_0f))**2)/L
    T[k] = (t_f)

    #print(E0-E00)

L = 18
print(L)
for k in range(0,l): 
    
    
    t_f = t0 + k*f1/l
    rg = (Jzf - Jz0)/t_f

    def ramp_g(t):
        return (Jz0 + rg*t)
    ramp_args=[]

    #basis = spin_basis_1d(L,pauli=False)
    basis = spin_basis_1d(L,pauli=False,zblock = +1,pblock = +1,kblock = 0)
    #basis = spin_basis_1d(L,pauli=False,Nup=L//2,zblock = +1,pblock = +1,kblock = 0)
    
    J_zz = [[Jzz_0,i,(i+1)%L] for i in range(L)] # PBC
    J_xy = [[Jxy/2.0,i,(i+1)%L] for i in range(L)] # PBC
    # J_zz = [[Jzz_0,i,i+1] for i in range(L-1)] # OBC
    # J_xy = [[Jxy/2.0,i,i+1] for i in range(L-1)] # OBC
    h_z=[[-hz*2,i] for i in range(L)]
# static and dynamic lists
    static = [["+-",J_xy],["-+",J_xy],["z",h_z]]
    static2 = [["+-",J_xy],["-+",J_xy]]
    dynamic =[["zz",J_zz,ramp_g,ramp_args]]
# compute the time-dependent Heisenberg Hamiltonian
    H_XXZ = hamiltonian(static,dynamic,basis=basis,dtype=np.float64,check_symm = False)
    H_XXZ_D = hamiltonian(static2,dynamic,basis=basis,dtype=np.float64,check_symm = False)
    #Emin0,Emax0=H_XXZ.eigsh(time=0.0, k=2,which="BE",maxiter=1E4,return_eigenvectors=False)
# calculate the eigenstate closest to energy E_star

    E0,psi_0=H_XXZ.eigsh(time = 0.0,k=1,which = "SA",maxiter=1E6)
    #EminF,EmaxF=H_XXZ.eigsh(time=t_f, k=2,which="BE",maxiter=1E4,return_eigenvectors=False,)
# calculate the eigenstate closest to energy E_star

    E00,psi_0f=H_XXZ.eigsh(time = t_f,k=1,which = "SA",maxiter=1E6)




    norm = np.linalg.norm(psi_0)
    psi_0 = psi_0/norm

    norm = np.linalg.norm(psi_0f)
    psi_0f = psi_0f/norm

    psi = H_XXZ_D.evolve(psi_0, 0.0 ,t_f)
    norm = np.linalg.norm(psi)
    psi = psi/norm


    # norm2 = np.linalg.norm(psi_0f)
    # psi_0f = psi_0f/norm2
    RF4[k] = -np.log(np.absolute(np.sum(np.conjugate(psi) * psi_0f))**2)/L
    T[k] = (t_f)

    #print(E0-E00)


# def func(z,a,b):
#     return a + b*z




# values,errors = curve_fit(func,np.log2(T),np.log2(RF))

# x = np.linspace(np.log2(e**s),np.log2(e**(l-1+s)),200)
# c,d= values
# #l1 = c/np.cbrt(x**2) 
# l1 = c + x*d 




# print(c,d)
# print(errors)
# # plt.plot(x,l1,'-b')

leg = ax.legend(prop={"size":30})
for label in (ax.get_xticklabels() + ax.get_yticklabels()):
	label.set_fontsize(16)
	
# plt.plot(np.log2(T),np.log2(RF),'--ro')
plt.plot((T),(RF1),'-go',label='N = '+str(L-6))
plt.plot((T),(RF2),'-bo',label='N = '+str(L-4))
plt.plot((T),(RF3),'-ro',label='N = '+str(L-2))
plt.plot((T),(RF4),'-yo',label='N = '+str(L))






ax.set_xlabel(r'$T$',fontsize = 30)
ax.set_ylabel(r'$f(T)$',fontsize = 30)
fig.suptitle( r'$J_z(t)$'+' ramps from '+str(Jz0)+' to ' +str(Jzf)+r'. $h_z$ = '+str(hz), fontsize=30, fontweight='bold')
#ax.set_title('axes title')

plt.legend(prop={'size':25})
plt.show()