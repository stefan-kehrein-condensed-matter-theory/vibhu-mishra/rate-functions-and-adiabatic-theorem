from quspin.operators import hamiltonian # Hamiltonians and operators
from quspin.basis import spin_basis_1d # Hilbert space spin basis
import numpy as np # generic math functions
from time import time
from quspin.tools.measurements import ent_entropy, diag_ensemble
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
fig = plt.figure()
ax = fig.add_subplot()




# This code is to check if the rate functions obtained are intensive or not.





hz=0.01*0 # z external field
Jxy = -1.0 # xy interaction
Jzz_0 = -1.0 # zz interaction
vs=np.logspace(-2.0, 0.0, num=20, base=10)


Jz0 = 0.9
Jzf = 1.1

s = 4
l = 2
global v

t_f = 3
v = (Jzf-Jz0)/t_f

RF =  [float(0.0) for y in range(s)]
TF =  [float(t_f) for y in range(s)]

def R_F(L):
    
    def ramp(t):
        return (Jz0 + v*t)
    ramp_args=[]

#ti = time() # get start time



# compute basis in the 0-total magnetisation sector (requires L even)
    basis = spin_basis_1d(L,pauli=False,Nup=L//2,zblock = +1,kblock = L,pblock = +1)
    #basis = spin_basis_1d(L,pauli=False)
# define operators with OBC using site-coupling lists
    J_zz = [[Jzz_0,i,(i+1)%L] for i in range(L)] # OBC
    J_xy = [[Jxy/2.0,i,(i+1)%L] for i in range(L)] # OBC
    h_z=[[-hz*2,i] for i in range(L)]
# static and dynamic lists
    static = [["+-",J_xy],["-+",J_xy],["z",h_z]]
    static2 = [["+-",J_xy],["-+",J_xy]]
    dynamic =[["zz",J_zz,ramp,ramp_args]]
# compute the time-dependent Heisenberg Hamiltonian
    H_XXZ = hamiltonian(static,dynamic,basis=basis,dtype=np.float64,check_symm = False)
    H_XXZ_D = hamiltonian(static2,dynamic,basis=basis,dtype=np.float64,check_symm = False)
    E0,psi_0=H_XXZ.eigsh(time = 0.0,k=1,which = "SA",maxiter=1E6)
    #EminF,EmaxF=H_XXZ.eigsh(time=t_f, k=2,which="BE",maxiter=1E4,return_eigenvectors=False,)
# calculate the eigenstate closest to energy E_star

    E00,psi_0f=H_XXZ.eigsh(time = t_f,k=1,which = "SA",maxiter=1E6)

#psi_0 = psi_0.reshape((-1,))
    norm = np.linalg.norm(psi_0)
    psi_0 = psi_0/norm

    psi = H_XXZ_D.evolve(psi_0, 0.0 ,t_f)
    norm1 = np.linalg.norm(psi)
    psi = psi/norm1
    
#psi_0f = psi_0.reshape((-1,))



    norm2 = np.linalg.norm(psi_0f)
    psi_0f = psi_0f/norm2
    return -np.log(np.absolute(np.sum(np.conjugate(psi) * psi_0f))**2)/L

for i in range(s):
    RF[i] = R_F(14+2*i)


Size = [16,18,20,22]


def funcE(z,a,b,c):
    return a*z*z + b*z + c

def funcI(z,d,e,f):
    return d + e/z + f/(z)**2



leg = ax.legend(prop={"size":30})
for label in (ax.get_xticklabels() + ax.get_yticklabels()):
	label.set_fontsize(16)


valuesE,errorsE = curve_fit(funcE,Size,RF)
a,b,c = valuesE
x = np.linspace(10,30,1000)

lE=a*x*x + b*x + c

print(a,b,c)
perrE = np.sqrt(np.diag(errorsE))

print(perrE)


valuesI,errorsI = curve_fit(funcI,Size,RF)
d,e,f = valuesI
#x = np.linspace(10,100,1000)

lI = d + e/x + f/(x)**2

print(d,e,f)
perrI = np.sqrt(np.diag(errorsI))

print(perrI)


box_style=dict(boxstyle='round', facecolor='wheat', alpha=0.5)
plt.plot(x, lI,label = 'Intensive',color='g')
plt.plot(x, lE,label = 'Extensive',color='b')
#ax.axvline(x=0, color='k')
plt.plot(Size,RF,'--ro',label = 'Rate Function')#label='L = 08')
#plt.text(0.125,0.00175, "$y=ax + b,  a = %s, b=%d$"%(a,b),{'color':'red','weight':'heavy','size':20},bbox=box_style)

ax.set_xlabel(r'$1/L$',fontsize = 30)
ax.set_ylabel(r'$f(T)$',fontsize = 30)
fig.suptitle(r'$J_z(t)$'+' goes from '+str(Jz0)+' to ' +str(Jzf)+r',  $T=$'+str(t_f), fontsize=30, fontweight='bold')  #a = $'+"{:.2e}".format(a)+'$\u00B1$'+"{:.2e}".format(perr[0])+', \n$b=$'+"{:.2e}".format(b)+'\u00B1'+"{:.2e}".format(perr[1])
#ax.set_title('axes title')

plt.legend(prop={'size':30})
plt.show()
