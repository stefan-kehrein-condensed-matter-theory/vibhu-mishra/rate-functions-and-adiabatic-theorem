from shutil import which
from quspin.operators import hamiltonian # Hamiltonians and operators
from quspin.basis import spin_basis_1d # Hilbert space spin basis
import numpy as np # generic math functions
from time import time
from quspin.tools.measurements import ent_entropy, diag_ensemble
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
fig = plt.figure()
ax = fig.add_subplot()



L = 18

hz=0.1*0 # z external field
Jxy = -1.0 # xy interaction
Jzz_0 = -1.0 # zz interaction
vs=np.logspace(-2.0, 0.0, num=20, base=10)


Jz0 = -2.0
Jzf = +2.0

T  = 100
global v
v = (Jzf-Jz0)/T
t_f = T



def ramp(t):
        return (t)
ramp_args=[]

#ti = time() # get start time



# compute basis in the 0-total magnetisation sector (requires L even)
#basis = spin_basis_1d(L,pauli=False,Nup = L//2)
basis = spin_basis_1d(L,pauli=False,kblock = 0,pblock = 1,zblock = 1,Nup = L//2)
    #basis = spin_basis_1d(L,pauli=False)
# define operators with OBC using site-coupling lists
J_zz = [[Jzz_0,i,(i+1)%L] for i in range(L)] # PBC
J_xy = [[Jxy/2.0,i,(i+1)%L] for i in range(L)] # PBC
h_z=[[hz,i] for i in range(1)]
# static and dynamic lists
static = [["+-",J_xy],["-+",J_xy],["z",h_z]]
dynamic =[["zz",J_zz,ramp,ramp_args]]
# compute the time-dependent Heisenberg Hamiltonian
H_XXZ = hamiltonian(static,dynamic,basis=basis,dtype=np.float64,check_symm = False)



# This code shows how the behaviour of the first 10 energy levels change when we vary J_z.



R = 10 #  Number of energy levels shown.

# RF =  [float(0.0) for y in range(s)]
# TF =  [float(t_f) for y in range(s)]

def Energy(t):
    
    
    #E = H_XXZ.eigsh(time = t, k = 5 ,which = 'SM',return_eigenvectors=False)
    E = H_XXZ.eigvalsh(time=t)
    E20 = E[0:R]

    return E20


# tStop = T
# tInc = 0.1
# t = np.arange(0., tStop, tInc)


#     Emin0,Emax0=H_XXZ.eigsh(time=0.0, k=2,which="BE",maxiter=1E4,return_eigenvectors=False)
# # calculate the eigenstate closest to energy E_star

#     E0,psi_0=H_XXZ.eigsh(time = 0.0,k=1,sigma=Emin0-1,maxiter=1E4)
# #psi_0 = psi_0.reshape((-1,))
#     norm = np.linalg.norm(psi_0)
#     psi_0 = psi_0/norm

#     psi = H_XXZ.evolve(psi_0, 0.0 ,t_f)
#     norm1 = np.linalg.norm(psi)
#     psi = psi/norm1
#     EminF,EmaxF=H_XXZ.eigsh(time=t_f, k=2,which="BE",maxiter=1E4,return_eigenvectors=False)
# # calculate the eigenstate closest to energy E_star

#     E00,psi_0f=H_XXZ.eigsh(time = t_f,k=1,sigma=EminF-1,maxiter=1E4)
# #psi_0f = psi_0.reshape((-1,))



    # norm2 = np.linalg.norm(psi_0f)
    # psi_0f = psi_0f/norm2
    # return -np.log(np.absolute(np.sum(np.conjugate(psi) * psi_0f)))/L

# for i in range(s):
#     RF[i] = R_F(12+2*i)


# InvSize = [1/12,1/14,1/16]


# def func(z,a,b):
#     return a*z+b


# values,errors = curve_fit(func,InvSize,RF)
# a,b = values
# x = np.linspace(-0.05,0.15,100)

# l=a*x+b

# print(a,b)
# perr = np.sqrt(np.diag(errors))


# box_style=dict(boxstyle='round', facecolor='wheat', alpha=0.5)
# plt.plot(x, l)
# ax.axvline(x=0, color='k')
# plt.plot(InvSize,RF,'--ro')#label='L = 08')
#plt.text(0.125,0.00175, "$y=ax + b,  a = %s, b=%d$"%(a,b),{'color':'red','weight':'heavy','size':20},bbox=box_style)
N = 10
EM = [[float(0.0) for i in range(N)]for j in range(int((Jzf-Jz0)*N))]
x = []

for i in range(0,int((Jzf-Jz0)*N)):
    ti = Jz0 + i/N
    EM[i] =  Energy(ti)
    x.append(ti)
    #np.concatenate(EM,Energy(ti),axis=1)
    
    #plt.plot(RF,Energy(ti),'ro')

#print(EM)

En = [float(0.0) for i in range(int((Jzf-Jz0)*N))]

print(EM)

for i in range(0,R):
    
    for j in range(0,int((Jzf-Jz0)*N)):
        En[j] = EM[j][i]
    # #ti = -2.0 + i/1
    #RF =  [float(ti) for y in range(10)]
    plt.plot(x,En,'-o')
    #plt.plot(x,En)


plt.annotate('Critical Point', xy=(+1, -4.5), xytext=(+0.9, -6.0),fontsize = 20, arrowprops=dict(facecolor='black', shrink=0.05))
plt.annotate(r'X.Y Ground state ', xy=(0, -5.8), xytext=(0, -6.8),fontsize = 20, arrowprops=dict(facecolor='black', shrink=0.05))
plt.annotate('Unique \n'+r' F.M G.S $<m_z> = 0$', xy=(+1.7, -6.25), xytext=(+1.0, -9.0),fontsize = 20, arrowprops=dict(facecolor='black', shrink=0.05))
#plt.annotate('A.F.M G.S', xy=(0.5, -5.66), xytext=(1.75, -5.66),fontsize = 20, arrowprops=dict(facecolor='black', shrink=0.05))
plt.annotate(r'Unique A.F.M G.S $<m_z> = 0$', xy=(-1.75, -10), xytext=(-0.5, -10),fontsize = 20, arrowprops=dict(facecolor='black', shrink=0.05))


ax.set_xlabel(r'$J_z$',fontsize = 30)
ax.set_ylabel('Energy',fontsize = 30)
fig.suptitle(r'$J_z$ ranges from ' + str(Jz0) + ' to +' + str(Jzf)+ '\n L = '+str(L), fontsize=30, fontweight='bold')
#ax.set_title('axes title')
plt.xticks(size = 20)
plt.yticks(size = 20)
plt.legend()
plt.show()